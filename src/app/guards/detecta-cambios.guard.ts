import { Injectable } from '@angular/core';
import { CanDeactivate } from '@angular/router';
import { Observable } from 'rxjs';
import { PuedeDesactivar } from './../interfaces/puede-desactivar'; 

@Injectable()
export class DetectaCambiosGuard implements CanDeactivate<PuedeDesactivar> {
	canDeactivate(component: PuedeDesactivar) {
		return component.comprobar(); 
	}
}
