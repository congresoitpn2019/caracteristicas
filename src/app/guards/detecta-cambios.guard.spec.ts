import { TestBed, async, inject } from '@angular/core/testing';

import { DetectaCambiosGuard } from './detecta-cambios.guard';

describe('DetectaCambiosGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [DetectaCambiosGuard]
    });
  });

  it('should ...', inject([DetectaCambiosGuard], (guard: DetectaCambiosGuard) => {
    expect(guard).toBeTruthy();
  }));
});
