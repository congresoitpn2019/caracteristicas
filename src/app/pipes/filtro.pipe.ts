import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filtro', 
  pure: true 
})
export class FiltroPipe implements PipeTransform {

  transform(value: Array<string>, texto: string): any {
    return value.filter(_ => _.lastIndexOf(texto) == -1);
  }

}
