import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'mayusculas'
})
export class MayusculasPipe implements PipeTransform {

  transform(value: string, mayus: boolean = true): any {
  	if (mayus)
    	return (<string> value).toUpperCase();
    else 
    	return (<string> value).toLowerCase();
  }

}
