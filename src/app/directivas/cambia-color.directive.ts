import { 
	Directive, 
	ElementRef, 
	OnInit, 
	HostListener, 
	Input 
} from '@angular/core';

@Directive({
  selector: '[appCambiaColor]'
})
export class CambiaColorDirective implements OnInit {
	@Input() colorA: string = 'yellow'; 
	@Input() colorB: string = 'blue';  

	constructor(private elementRef: ElementRef) { }

	ngOnInit() {
  		this.elementRef.nativeElement.style.backgroundColor = this.colorA;
  	}

	@HostListener('mouseover') onMouseEnter() {
		console.log('over');
		this.elementRef.nativeElement.style.backgroundColor = this.colorB; 
	}

	@HostListener('mouseout') onMouseLeave() {
		console.log('out');
		this.elementRef.nativeElement.style.backgroundColor = this.colorA; 
	}
}
