import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router'; 
import { FormsModule } from '@angular/forms'; 

import { AppComponent } from './app.component';
import { EjemploDirectivasComponent } from './componentes/ejemplo-directivas/ejemplo-directivas.component';
import { EjemploServiciosComponent } from './componentes/ejemplo-servicios/ejemplo-servicios.component';
import { EjemploGuardsComponent } from './componentes/ejemplo-guards/ejemplo-guards.component';
import { EjemploPipesComponent } from './componentes/ejemplo-pipes/ejemplo-pipes.component';
import { HeaderTiendaComponent } from './componentes/header-tienda/header-tienda.component';

import { CambiaColorDirective } from './directivas/cambia-color.directive';
import { MonedasService } from './servicios/monedas.service';
import { MayusculasPipe } from './pipes/mayusculas.pipe';
import { FiltroPipe } from './pipes/filtro.pipe';
import { DetectaCambiosGuard } from './guards/detecta-cambios.guard';


const appRoutes: Routes = [
  {
    path: '', 
    redirectTo: '/directivas', 
    pathMatch: 'full'
  }, 
  {
    path: 'directivas', 
    component: EjemploDirectivasComponent
  }, 
  {
    path: 'servicios', 
    component: EjemploServiciosComponent
  }, 
  {
    path: 'pipes', 
    component: EjemploPipesComponent
  }, 
  {
    path: 'guards', 
    component: EjemploGuardsComponent
  }
]

@NgModule({
  declarations: [
    AppComponent,
    EjemploDirectivasComponent,
    EjemploServiciosComponent,
    EjemploGuardsComponent,
    EjemploPipesComponent,
    MayusculasPipe,
    FiltroPipe,
    CambiaColorDirective,
    HeaderTiendaComponent
  ],
  imports: [
    BrowserModule, 
    RouterModule.forRoot(appRoutes), 
    FormsModule
  ],
  providers: [MonedasService, DetectaCambiosGuard],
  bootstrap: [AppComponent]
})
export class AppModule { }
