import { TestBed, inject } from '@angular/core/testing';

import { MonedasService } from './monedas.service';

describe('MonedasService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [MonedasService]
    });
  });

  it('should be created', inject([MonedasService], (service: MonedasService) => {
    expect(service).toBeTruthy();
  }));
});
