import { Injectable, EventEmitter } from '@angular/core';

@Injectable()
export class MonedasService {
	private actual = 'MXP'; 
	public cambia = new EventEmitter<string>(); 



  constructor() { }

  public set moneda(valor: string) {
  	if (this.actual == valor) { return; }
  	this.actual = valor; 
  	this.cambia.next(valor); 
  }

  public get moneda(): string {
  	return this.actual;
  }

}
