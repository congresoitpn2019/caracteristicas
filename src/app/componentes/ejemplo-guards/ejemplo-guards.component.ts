import { Component, OnInit } from '@angular/core';
import { PuedeDesactivar } from './../../interfaces/puede-desactivar'; 


@Component({
  selector: 'app-ejemplo-guards',
  templateUrl: './ejemplo-guards.component.html',
  styleUrls: ['./ejemplo-guards.component.css']
})
export class EjemploGuardsComponent implements OnInit, PuedeDesactivar {
	public nombre = 'Gerardo Yáñez';
	private original = ''; 

	constructor() { }

	ngOnInit() {
		this.original = this.nombre; 
	}

	comprobar(): boolean {
		console.log(this.nombre == this.original); 

		if (this.nombre != this.original) {
			return confirm('Los datos se han alterado ¿Seguro?')
		}
		else return true;
	}

}
