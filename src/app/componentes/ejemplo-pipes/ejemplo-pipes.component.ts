import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-ejemplo-pipes',
  templateUrl: './ejemplo-pipes.component.html',
  styleUrls: ['./ejemplo-pipes.component.css']
})
export class EjemploPipesComponent implements OnInit {
	public monto = 78.23000; 
	public fecha = new Date(); 
	public tipoCambio = 19.35; 
	public nombre = 'Gerardo Yáñez'; 

	public lista = [ 'Manzana', 'Pera', 'Piña' ]; 


	constructor() { }

	ngOnInit() {
	}

	public agregar(elem: any) {
		console.log(elem.value); 
		this.lista.push(elem.value); 
	}
}
