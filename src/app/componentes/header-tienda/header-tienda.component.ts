import { Component, OnInit } from '@angular/core';
import { MonedasService } from './../../servicios/monedas.service'; 

@Component({
  selector: 'app-header-tienda',
  templateUrl: './header-tienda.component.html',
  styleUrls: ['./header-tienda.component.css']
})
export class HeaderTiendaComponent implements OnInit {
	public moneda = ''; 


	constructor(public monedasSrv: MonedasService) { }

	ngOnInit() {
		this.moneda = this.monedasSrv.moneda;
		this.monedasSrv.cambia.subscribe(val => {
			this.moneda = val; 
		}); 
	}

}
