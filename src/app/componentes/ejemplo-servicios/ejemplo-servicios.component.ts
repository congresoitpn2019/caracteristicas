import { Component, OnInit } from '@angular/core'; 
import { MonedasService } from './../../servicios/monedas.service'; 


@Component({
  selector: 'app-ejemplo-servicios',
  templateUrl: './ejemplo-servicios.component.html',
  styleUrls: ['./ejemplo-servicios.component.css']
})
export class EjemploServiciosComponent implements OnInit {
	public moneda = "USD";

	constructor(public monedasSrv: MonedasService) { }

	ngOnInit() {
		this.moneda = this.monedasSrv.moneda; 
	}

	public moneda_onChange(val: string) {
		this.moneda = val; 
		this.monedasSrv.moneda = this.moneda; 
	}

}
